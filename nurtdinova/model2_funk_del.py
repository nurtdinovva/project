import matplotlib.pyplot as plt
import numpy as np
import scipy.integrate


def func(u, del0, tau = 800, alpha = 5.0, lamb = 20.0, c1 = 40, c2 = 100, c3 = 50, d1 = 200, d2 = 220):
    
    
    f1 = lambda t: (alpha * d1 - c1) * (tau - alpha * t) 
    f2 = lambda t: (- alpha * d2 * (10 + (tau - alpha * t)/100) + c2 * (5 - (tau - alpha * t)/5000)) * (tau - alpha * t)
    f3 = lambda t: c1 * (10 - (tau - alpha * (u + del0) + lamb * (t - u - del0))/500) * (tau - alpha * (u + del0) + lamb * (t - u - del0))
    f4 = lambda t: - c2 * (5 - (tau - alpha * (u + del0) + lamb * (t - u - del0))/5000) * (tau - alpha * (u + del0) + lamb * (t - u - del0))
    
    
    
    S = 0
    S, _ = scipy.integrate.quad(f1, 0, u + del0)
    
    if (u <= tau / alpha - del0):
        S, _ = scipy.integrate.quad(f1, 0, u + del0)
        s1, _ = scipy.integrate.quad(f3, u + del0, (1 + alpha / lamb) * (u + del0))
        S -= s1 + c3 * (alpha * (u + del0))
        
    if (u > tau / alpha - del0):
        S, _ = scipy.integrate.quad(f1, 0, tau / alpha)
        s2, _ = scipy.integrate.quad(f2, tau / alpha, u + del0)
        s3, _ = scipy.integrate.quad(f4, u + del0, (1 + alpha / lamb) * (u + del0) - tau / lamb)
        s4, _ = scipy.integrate.quad(f3, (1 + alpha / lamb) * (u + del0) - tau / lamb, (1 + alpha / lamb) * (u + del0))
        S += s2 - s3 - s4 - c3 * (alpha * (u + del0))
        
    S /= (1 + alpha / lamb) * (u + del0)
    
    return S

print()

u_max = 350
num = 200
U = np.linspace(0, u_max, num)

fig, ax = plt.subplots(nrows=1,ncols=1, figsize=(16,9))

for delt in range(10, 150, 30):
    f_max = 0
    u_best = 0
    f_min = func(u = U[1], del0 = delt)
    u_worst = 0
    F = np.zeros(num)    
    for i in range(U.size):
        F[i] = func(u = U[i], del0 = delt)
        if (f_max < F[i]):
            f_max = F[i]
            u_best = U[i]
        if (f_min > F[i]):
            f_min = F[i]
            u_worst = U[i]
    print('Задержка равна', delt,'\nЗначение основной функции:',f_max,'\nПараметр управления:', u_best)
    #print('Худший параметр управления:', u_worst, '\n')
    plt.scatter(u_best, f_max, color='darkmagenta', s = 40, marker='o')
    ax.plot(U,F, label='$\Delta$ = {:.2f}'.format(delt))
ax.plot(u_best, f_max, label='Максимумы',color='darkmagenta', marker = 'o', linestyle="None")
ax.legend(loc='best')

fig.suptitle('Графики поведения основной функции', fontsize=24)
plt.xlabel('Параметр управления u, ч', fontsize=18)
plt.ylabel('Значение функции С(u), млн', fontsize=18)
ax.grid()

plt.show()


#лучшее u и delta0
# UU = np.linspace(0, 250, 100)
# DEL = np.linspace(10, 150, 100)

# max = 0

# main = []
# for i in range(UU.size):
#     for y in range(DEL.size):
#         main.append(func(u=UU[i], del0 = DEL[y]))
#         if max < func(u=UU[i], del0 = DEL[y]):
#             max = func(u=UU[i], del0 = DEL[y])
#             i_max = i
#             y_max = y
# main.sort()
# print(main[-1], UU[i_max], DEL[y_max])






DELT = np.linspace(10, 150, num)
F_max = np.zeros(num)
U_best = np.zeros(num)

for i in range(DELT.size):
    f = np.zeros(num)    
    for y in range(U.size):
        f[y] = func(u = U[y], del0 = DELT[i])
        if (F_max[i] < f[y]):
            F_max[i] = f[y]
            U_best[i] = U[y]

fig, ax = plt.subplots(nrows=1,ncols=1, figsize=(16,9))

ax.plot(DELT, U_best, c = 'deeppink') 

fig.suptitle('Зависимость оптимального параметра управления от величины задержки', fontsize=24)
plt.xlabel('Величина задержки, ч', fontsize=18)
plt.ylabel('Величина оптимального параметра управления, ч', fontsize=18)
ax.grid()

plt.show()

fig, ax = plt.subplots(nrows=1, ncols=1, figsize=(16,9))

ax.plot(DELT, F_max, c = 'deeppink') 

fig.suptitle('Зависимость максимума основной функции от величины задержки', fontsize=24)
plt.xlabel('Величина задержки, ч', fontsize=18)
plt.ylabel('Значение максимума функции С(u), млн', fontsize=18)
ax.grid()

plt.show()






# FUNC = [func(u = UU[i], del0 = DEL[i]) for i in range(100)]
# maxFUNC = max(FUNC)
# maxI = FUNC.index(maxFUNC)


# fig = plt.figure()
# ax = fig.add_subplot(111, projection='3d')
# ax.scatter(UU, DEL, FUNC, label='parametric curve', marker='.')
# ax.scatter(UU[maxI], DEL[maxI], maxFUNC, color='darkmagenta', s = 40, marker='o')
# print('Задержка равна', DEL[maxI], '\nПараметр управления равен', UU[maxI],'\nЗначение основной функции:',maxFUNC)
# plt.show()
