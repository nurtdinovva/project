Notebook[{Cell[
CellGroupData[{Cell[
BoxData[RowBox[{"A1","=",RowBox[{RowBox[{"Integrate","[",RowBox[
{RowBox[{RowBox[{"(",RowBox[{"\[Alpha]","*","\n","d1","*",RowBox[{"(",RowBox[{"\[Tau]","-",RowBox[
{"\[Alpha]","*","t"}]}],")"}]}],")"}],"-",RowBox[{"c1","*",RowBox[{"(",RowBox[{"\[Tau]","-",RowBox[
{"\[Alpha]","*","t"}]}],")"}]}]}],",",RowBox[{"{",RowBox[{"t",",","0",",",RowBox[
{"u","+","\[CapitalDelta]"}]}],"}"}]}],"]"}],"-",RowBox[{"c1","*",RowBox[{"(",RowBox[
{"\[Tau]","-",RowBox[{"\[Alpha]","*",RowBox[{"(",RowBox[{"u","-","\[CapitalDelta]"}],")"}]}]}],")"}],"*",RowBox[
{"(",RowBox[{"u","+","\[CapitalDelta]"}],")"}],"*",RowBox[{"(",RowBox[{"\[Alpha]","/","\[Lambda]"}],")"}]}],"-",RowBox[
{"c3","*",RowBox[{"(",RowBox[{"\[Alpha]","*",RowBox[{"(",RowBox[{"u","+","\[CapitalDelta]"}],")"}]}],")"}]}]}]}]],
"Input",CellLabel -> "In[11]:= ",ExpressionUUID -> "551e98bb-2dcd-45c5-8638-1f09a2485c9e"],Cell[
BoxData[
RowBox[{RowBox[{RowBox[{"-","c3"}]," ","\[Alpha]"," ",RowBox[{"(",RowBox[{"u","+","\[CapitalDelta]"}],")"}]}],"+",RowBox[
{FractionBox["1","2"]," ",RowBox[{"(",RowBox[{"c1","-",RowBox[{"d1"," ","\[Alpha]"}]}],")"}]," ",RowBox[
{"(",RowBox[{"u","+","\[CapitalDelta]"}],")"}]," ",RowBox[{"(",RowBox[{RowBox[{"u"," ","\[Alpha]"}],"+",RowBox[
{"\[Alpha]"," ","\[CapitalDelta]"}],"-",RowBox[{"2"," ","\[Tau]"}]}],")"}]}],"-",FractionBox[
RowBox[{"c1"," ","\[Alpha]"," ",RowBox[{"(",RowBox[{"u","+","\[CapitalDelta]"}],")"}]," ",RowBox[
{"(",RowBox[{RowBox[{RowBox[{"-","\[Alpha]"}]," ",RowBox[{"(",RowBox[{"u","-","\[CapitalDelta]"}],")"}]}],"+","\[Tau]"}],")"}]}],
"\[Lambda]"]}],StandardForm],"Output",CellLabel -> "Out[11]= ",ExpressionUUID -> "08630c9f-7890-49a7-9be8-365a9e346965"]},
Open],ExpressionUUID -> "22a17c16-a27d-4f55-b9b1-9b5ff845a41d"],Cell[
CellGroupData[
{Cell[
BoxData[RowBox[{"A2","="," ",RowBox[{RowBox[{"Integrate","[",RowBox[{RowBox[
{"(",RowBox[{RowBox[{"\[Alpha]","*","d1","*",RowBox[{"(",RowBox[{"\[Tau]","-",RowBox[
{"\[Alpha]","*","t"}]}],")"}]}],"-",RowBox[{"c1","*",RowBox[{"(",RowBox[{"\[Tau]","-",RowBox[
{"\[Alpha]","*","t"}]}],")"}]}]}],")"}],",",RowBox[{"{",RowBox[{"t",","," ","0",",",RowBox[
{"\[Tau]","/","\[Alpha]"}]}]," ","}"}]}],"]"}],"-",RowBox[{"Integrate","[",RowBox[
{RowBox[{RowBox[{"\[Alpha]","*","d2","*",RowBox[{"(",RowBox[{"\[Tau]","-",RowBox[
{"\[Alpha]","*","t"}]}],")"}]}],"-",RowBox[{"c2","*",RowBox[{"(",RowBox[{"\[Tau]","-",RowBox[
{"\[Alpha]","*","t"}]}],")"}]}]}]," ",",",RowBox[{"{",RowBox[{"t",",",RowBox[{"\[Tau]","/","\[Alpha]"}],",",RowBox[
{"u","+","\[CapitalDelta]"}]}]," ","}"}]}],"]"}]," ","+"," ",RowBox[{"c2","*",RowBox[
{"(",RowBox[{"\[Tau]","-",RowBox[{"\[Alpha]","*",RowBox[{"(",RowBox[{"u","+","\[CapitalDelta]"}],")"}]}]}],")"}],"*",RowBox[
{"(",RowBox[{"\[Alpha]","/","\[Lambda]"}],")"}],"*",RowBox[{"(",RowBox[{"u","+","\[CapitalDelta]"}],")"}]}],"-",RowBox[
{"c3","*",RowBox[{"(",RowBox[{"\[Alpha]","*",RowBox[{"(",RowBox[{"u","+","\[CapitalDelta]"}],")"}]}],")"}]}]}]}]],
"Input",CellLabel -> "In[2]:= ",ExpressionUUID -> "2ecbb5f1-0261-44f8-9c45-47f307de75b4"],Cell[
BoxData[
RowBox[{RowBox[{RowBox[{"-",FractionBox["1","2"]}]," ","c2"," ",SuperscriptBox["u",
"2"]," ","\[Alpha]"}],"+",RowBox[{FractionBox["1","2"]," ","d2"," ",SuperscriptBox[
"u","2"]," ",SuperscriptBox["\[Alpha]","2"]}],"-",RowBox[{"c2"," ","u"," ","\[Alpha]"," ","\[CapitalDelta]"}],"+",RowBox[
{"d2"," ","u"," ",SuperscriptBox["\[Alpha]","2"]," ","\[CapitalDelta]"}],"-",RowBox[
{FractionBox["1","2"]," ","c2"," ","\[Alpha]"," ",SuperscriptBox["\[CapitalDelta]",
"2"]}],"+",RowBox[{FractionBox["1","2"]," ","d2"," ",SuperscriptBox["\[Alpha]","2"]," ",SuperscriptBox[
"\[CapitalDelta]","2"]}],"-",RowBox[{"c3"," ","\[Alpha]"," ",RowBox[{"(",RowBox[{"u","+","\[CapitalDelta]"}],")"}]}],"+",RowBox[
{"c2"," ","u"," ","\[Tau]"}],"-",RowBox[{"d2"," ","u"," ","\[Alpha]"," ","\[Tau]"}],"+",RowBox[
{"c2"," ","\[CapitalDelta]"," ","\[Tau]"}],"-",RowBox[{"d2"," ","\[Alpha]"," ","\[CapitalDelta]"," ","\[Tau]"}],"+",FractionBox[
RowBox[{"d1"," ",SuperscriptBox["\[Tau]","2"]}],"2"],"+",FractionBox[RowBox[{"d2"," ",SuperscriptBox[
"\[Tau]","2"]}],"2"],"-",FractionBox[RowBox[{"c1"," ",SuperscriptBox["\[Tau]","2"]}],
RowBox[{"2"," ","\[Alpha]"}]],"-",FractionBox[RowBox[{"c2"," ",SuperscriptBox["\[Tau]",
"2"]}],RowBox[{"2"," ","\[Alpha]"}]],"+",FractionBox[RowBox[{"c2"," ","\[Alpha]"," ",RowBox[
{"(",RowBox[{"u","+","\[CapitalDelta]"}],")"}]," ",RowBox[{"(",RowBox[{RowBox[{RowBox[
{"-","\[Alpha]"}]," ",RowBox[{"(",RowBox[{"u","+","\[CapitalDelta]"}],")"}]}],"+","\[Tau]"}],")"}]}],
"\[Lambda]"]}],StandardForm],"Output",CellLabel -> "Out[2]= ",ExpressionUUID -> "19bfb2de-848a-430a-a69a-43342c810214"]},
Open],ExpressionUUID -> "f0f1edb9-55b2-4469-a5c2-7b1f0ced749c"],Cell[
CellGroupData[
{Cell[
BoxData[RowBox[{"B","="," ",RowBox[{RowBox[{"("," ",RowBox[{"u","+","\[CapitalDelta]"}],")"}],"/",RowBox[
{"(",RowBox[{"1","+",RowBox[{"(",RowBox[{"\[Alpha]","/","\[Lambda]"}],")"}]}],")"}]}]}]],
"Input",CellLabel -> "In[3]:= ",ExpressionUUID -> "2f083471-8cb0-4b5b-bfa1-ecf8665e1916"],Cell[
BoxData[
FractionBox[RowBox[{"u","+","\[CapitalDelta]"}],RowBox[{"1","+",FractionBox["\[Alpha]",
"\[Lambda]"]}]],StandardForm],"Output",CellLabel -> "Out[3]= ",ExpressionUUID -> "b2330db8-5eb2-4874-8a1c-92e662a04b31"]},
Open],ExpressionUUID -> "307c2471-2dac-4058-b65a-8a90dfc1f2b4"],Cell[
CellGroupData[
{Cell[
BoxData[RowBox[{"C1","=",RowBox[{"A1","/","B"}]}]],"Input",CellLabel -> "In[4]:= ",
ExpressionUUID -> "4ae1c47b-f7e3-4ff0-9175-bd3386d3550d"],Cell[
BoxData[FractionBox[
RowBox[{RowBox[{"(",RowBox[{"1","+",FractionBox["\[Alpha]","\[Lambda]"]}],")"}]," ",RowBox[
{"(",RowBox[{RowBox[{RowBox[{"-","c3"}]," ","\[Alpha]"," ",RowBox[{"(",RowBox[{"u","+","\[CapitalDelta]"}],")"}]}],"+",RowBox[
{FractionBox["1","2"]," ",RowBox[{"(",RowBox[{"c1","-",RowBox[{"d1"," ","\[Alpha]"}]}],")"}]," ",RowBox[
{"(",RowBox[{"u","+","\[CapitalDelta]"}],")"}]," ",RowBox[{"(",RowBox[{RowBox[{"u"," ","\[Alpha]"}],"+",RowBox[
{"\[Alpha]"," ","\[CapitalDelta]"}],"-",RowBox[{"2"," ","\[Tau]"}]}],")"}]}],"-",FractionBox[
RowBox[{"c1"," ","\[Alpha]"," ",RowBox[{"(",RowBox[{"u","+","\[CapitalDelta]"}],")"}]," ",RowBox[
{"(",RowBox[{RowBox[{RowBox[{"-","\[Alpha]"}]," ",RowBox[{"(",RowBox[{"u","-","\[CapitalDelta]"}],")"}]}],"+","\[Tau]"}],")"}]}],
"\[Lambda]"]}],")"}]}],RowBox[{"u","+","\[CapitalDelta]"}]],StandardForm],"Output",
CellLabel -> "Out[4]= ",ExpressionUUID -> "2b542512-312e-4246-b088-fa788df1d9ad"]},
Open],ExpressionUUID -> "5a688cf7-61ae-4130-a11d-83289194c673"],Cell[
CellGroupData[
{Cell[
BoxData[RowBox[{"FullSimplify","[","C1","]"}]],"Input",CellLabel -> "In[5]:= ",
ExpressionUUID -> "792c8575-17ac-4c75-b763-0dd9e0489e9e"],Cell[
BoxData[FractionBox[
RowBox[{RowBox[{"(",RowBox[{"\[Alpha]","+","\[Lambda]"}],")"}]," ",RowBox[{"(",RowBox[
{RowBox[{"\[Alpha]"," ",RowBox[{"(",RowBox[{RowBox[{"2"," ","c1"," ","\[Alpha]"," ",RowBox[
{"(",RowBox[{"u","-","\[CapitalDelta]"}],")"}]}],"-",RowBox[{"2"," ","c3"," ","\[Lambda]"}],"+",RowBox[
{RowBox[{"(",RowBox[{"c1","-",RowBox[{"d1"," ","\[Alpha]"}]}],")"}]," ",RowBox[{"(",RowBox[
{"u","+","\[CapitalDelta]"}],")"}]," ","\[Lambda]"}]}],")"}]}],"+",RowBox[{"2"," ","d1"," ","\[Alpha]"," ","\[Lambda]"," ","\[Tau]"}],"-",RowBox[
{"2"," ","c1"," ",RowBox[{"(",RowBox[{"\[Alpha]","+","\[Lambda]"}],")"}]," ","\[Tau]"}]}],")"}]}],
RowBox[{"2"," ",SuperscriptBox["\[Lambda]","2"]}]],StandardForm],"Output",CellLabel -> "Out[5]= ",
ExpressionUUID -> "d6eb2337-5cc6-4de6-b735-72ffec5a466c"]},Open],ExpressionUUID -> "edd324c7-dfeb-4aaa-ab7e-f3ff7f188c34"],Cell[
CellGroupData[
{Cell[
BoxData[RowBox[{"C2","=",RowBox[{"A2","/","B"}]}]],"Input",CellLabel -> "In[6]:= ",
ExpressionUUID -> "629d5b74-de48-4e2d-8eca-e197ccc74bc3"],Cell[
BoxData[FractionBox[
RowBox[{RowBox[{"(",RowBox[{"1","+",FractionBox["\[Alpha]","\[Lambda]"]}],")"}]," ",RowBox[
{"(",RowBox[{RowBox[{RowBox[{"-",FractionBox["1","2"]}]," ","c2"," ",SuperscriptBox[
"u","2"]," ","\[Alpha]"}],"+",RowBox[{FractionBox["1","2"]," ","d2"," ",SuperscriptBox[
"u","2"]," ",SuperscriptBox["\[Alpha]","2"]}],"-",RowBox[{"c2"," ","u"," ","\[Alpha]"," ","\[CapitalDelta]"}],"+",RowBox[
{"d2"," ","u"," ",SuperscriptBox["\[Alpha]","2"]," ","\[CapitalDelta]"}],"-",RowBox[
{FractionBox["1","2"]," ","c2"," ","\[Alpha]"," ",SuperscriptBox["\[CapitalDelta]",
"2"]}],"+",RowBox[{FractionBox["1","2"]," ","d2"," ",SuperscriptBox["\[Alpha]","2"]," ",SuperscriptBox[
"\[CapitalDelta]","2"]}],"-",RowBox[{"c3"," ","\[Alpha]"," ",RowBox[{"(",RowBox[{"u","+","\[CapitalDelta]"}],")"}]}],"+",RowBox[
{"c2"," ","u"," ","\[Tau]"}],"-",RowBox[{"d2"," ","u"," ","\[Alpha]"," ","\[Tau]"}],"+",RowBox[
{"c2"," ","\[CapitalDelta]"," ","\[Tau]"}],"-",RowBox[{"d2"," ","\[Alpha]"," ","\[CapitalDelta]"," ","\[Tau]"}],"+",FractionBox[
RowBox[{"d1"," ",SuperscriptBox["\[Tau]","2"]}],"2"],"+",FractionBox[RowBox[{"d2"," ",SuperscriptBox[
"\[Tau]","2"]}],"2"],"-",FractionBox[RowBox[{"c1"," ",SuperscriptBox["\[Tau]","2"]}],
RowBox[{"2"," ","\[Alpha]"}]],"-",FractionBox[RowBox[{"c2"," ",SuperscriptBox["\[Tau]",
"2"]}],RowBox[{"2"," ","\[Alpha]"}]],"+",FractionBox[RowBox[{"c2"," ","\[Alpha]"," ",RowBox[
{"(",RowBox[{"u","+","\[CapitalDelta]"}],")"}]," ",RowBox[{"(",RowBox[{RowBox[{RowBox[
{"-","\[Alpha]"}]," ",RowBox[{"(",RowBox[{"u","+","\[CapitalDelta]"}],")"}]}],"+","\[Tau]"}],")"}]}],
"\[Lambda]"]}],")"}]}],RowBox[{"u","+","\[CapitalDelta]"}]],StandardForm],"Output",
CellLabel -> "Out[6]= ",ExpressionUUID -> "d7075415-b426-47b2-b744-0438d366aea4"]},
Open],ExpressionUUID -> "5c0da5b9-b135-4988-bd58-f82e29a2b37c"],Cell[
CellGroupData[
{Cell[
BoxData[RowBox[{"FullSimplify","[","C2","]"}]],"Input",CellLabel -> "In[7]:= ",
ExpressionUUID -> "69d14542-7710-451a-b76e-01975dbc4aae"],Cell[
BoxData[FractionBox[
RowBox[{RowBox[{"(",RowBox[{"\[Alpha]","+","\[Lambda]"}],")"}]," ",RowBox[{"(",RowBox[
{RowBox[{"c2"," ",RowBox[{"(",RowBox[{RowBox[{RowBox[{"-",SuperscriptBox["\[Alpha]",
"2"]}]," ",SuperscriptBox[RowBox[{"(",RowBox[{"u","+","\[CapitalDelta]"}],")"}],"2"]," ",RowBox[
{"(",RowBox[{RowBox[{"2"," ","\[Alpha]"}],"+","\[Lambda]"}],")"}]}],"+",RowBox[{"2"," ","\[Alpha]"," ",RowBox[
{"(",RowBox[{"u","+","\[CapitalDelta]"}],")"}]," ",RowBox[{"(",RowBox[{"\[Alpha]","+","\[Lambda]"}],")"}]," ","\[Tau]"}],"-",RowBox[
{"\[Lambda]"," ",SuperscriptBox["\[Tau]","2"]}]}],")"}]}],"+",RowBox[{"\[Lambda]"," ",RowBox[
{"(",RowBox[{RowBox[{RowBox[{"-","2"}]," ","c3"," ",SuperscriptBox["\[Alpha]","2"]," ",RowBox[
{"(",RowBox[{"u","+","\[CapitalDelta]"}],")"}]}],"+",RowBox[{RowBox[{"(",RowBox[{RowBox[
{"-","c1"}],"+",RowBox[{"d1"," ","\[Alpha]"}]}],")"}]," ",SuperscriptBox["\[Tau]",
"2"]}],"+",RowBox[{"d2"," ","\[Alpha]"," ",SuperscriptBox[RowBox[{"(",RowBox[{RowBox[
{RowBox[{"-","\[Alpha]"}]," ",RowBox[{"(",RowBox[{"u","+","\[CapitalDelta]"}],")"}]}],"+","\[Tau]"}],")"}],
"2"]}]}],")"}]}]}],")"}]}],RowBox[{"2"," ","\[Alpha]"," ",RowBox[{"(",RowBox[{"u","+","\[CapitalDelta]"}],")"}]," ",SuperscriptBox[
"\[Lambda]","2"]}]],StandardForm],"Output",CellLabel -> "Out[7]= ",ExpressionUUID -> "0c03333d-7fa1-4f6c-b9ed-3f5bdf1fac8e"]},
Open],ExpressionUUID -> "d2025888-47f5-4cae-b5fc-626bce3f4f26"],Cell[
CellGroupData[
{Cell[
BoxData[RowBox[{"D","[",RowBox[{"C1",",","\[Lambda]"}],"]"}]],"Input",CellLabel -> "In[8]:= ",
ExpressionUUID -> "9c4645f0-f5dd-4e7e-82d4-952f800b646f"],Cell[
BoxData[RowBox[{FractionBox[
RowBox[{"c1"," ","\[Alpha]"," ",RowBox[{"(",RowBox[{"1","+",FractionBox["\[Alpha]",
"\[Lambda]"]}],")"}]," ",RowBox[{"(",RowBox[{RowBox[{RowBox[{"-","\[Alpha]"}]," ",RowBox[
{"(",RowBox[{"u","-","\[CapitalDelta]"}],")"}]}],"+","\[Tau]"}],")"}]}],SuperscriptBox[
"\[Lambda]","2"]],"-",FractionBox[RowBox[{"\[Alpha]"," ",RowBox[{"(",RowBox[{RowBox[
{RowBox[{"-","c3"}]," ","\[Alpha]"," ",RowBox[{"(",RowBox[{"u","+","\[CapitalDelta]"}],")"}]}],"+",RowBox[
{FractionBox["1","2"]," ",RowBox[{"(",RowBox[{"c1","-",RowBox[{"d1"," ","\[Alpha]"}]}],")"}]," ",RowBox[
{"(",RowBox[{"u","+","\[CapitalDelta]"}],")"}]," ",RowBox[{"(",RowBox[{RowBox[{"u"," ","\[Alpha]"}],"+",RowBox[
{"\[Alpha]"," ","\[CapitalDelta]"}],"-",RowBox[{"2"," ","\[Tau]"}]}],")"}]}],"-",FractionBox[
RowBox[{"c1"," ","\[Alpha]"," ",RowBox[{"(",RowBox[{"u","+","\[CapitalDelta]"}],")"}]," ",RowBox[
{"(",RowBox[{RowBox[{RowBox[{"-","\[Alpha]"}]," ",RowBox[{"(",RowBox[{"u","-","\[CapitalDelta]"}],")"}]}],"+","\[Tau]"}],")"}]}],
"\[Lambda]"]}],")"}]}],RowBox[{RowBox[{"(",RowBox[{"u","+","\[CapitalDelta]"}],")"}]," ",SuperscriptBox[
"\[Lambda]","2"]}]]}],StandardForm],"Output",CellLabel -> "Out[8]= ",ExpressionUUID -> "5115618c-8ce0-40eb-a28a-b17ffb2408fb"]},
Open],ExpressionUUID -> "a4573aad-c933-4541-a5fd-c9c87c8cb5ff"],Cell[
CellGroupData[
{Cell[
BoxData[RowBox[{"D","[",RowBox[{"C2",","," ","\[Lambda]"}],"]"}]],"Input",
CellLabel -> "In[13]:= ",ExpressionUUID -> "df32cf7a-f9ea-4af8-b882-325d4771bfcd"],Cell[
BoxData[
RowBox[{RowBox[{"-",FractionBox[RowBox[{"c2"," ","\[Alpha]"," ",RowBox[{"(",RowBox[
{"1","+",FractionBox["\[Alpha]","\[Lambda]"]}],")"}]," ",RowBox[{"(",RowBox[{RowBox[
{RowBox[{"-","\[Alpha]"}]," ",RowBox[{"(",RowBox[{"u","+","\[CapitalDelta]"}],")"}]}],"+","\[Tau]"}],")"}]}],
SuperscriptBox["\[Lambda]","2"]]}],"-",FractionBox[RowBox[{"\[Alpha]"," ",RowBox[
{"(",RowBox[{RowBox[{RowBox[{"-","c3"}]," ","\[Alpha]"," ",RowBox[{"(",RowBox[{"u","+","\[CapitalDelta]"}],")"}]}],"+",FractionBox[
RowBox[{"d1"," ",SuperscriptBox["\[Tau]","2"]}],"2"],"-",FractionBox[RowBox[{"c1"," ",SuperscriptBox[
"\[Tau]","2"]}],RowBox[{"2"," ","\[Alpha]"}]],"+",FractionBox[RowBox[{"c2"," ","\[Alpha]"," ",RowBox[
{"(",RowBox[{"u","+","\[CapitalDelta]"}],")"}]," ",RowBox[{"(",RowBox[{RowBox[{RowBox[
{"-","\[Alpha]"}]," ",RowBox[{"(",RowBox[{"u","+","\[CapitalDelta]"}],")"}]}],"+","\[Tau]"}],")"}]}],
"\[Lambda]"],"+",RowBox[{FractionBox["1","6"]," ","d2"," ",RowBox[{"(",RowBox[{RowBox[
{"3"," ","c2"}],"+",RowBox[{"10"," ","\[Alpha]"," ",RowBox[{"(",RowBox[{"u","+","\[CapitalDelta]"}],")"}]}],"-",RowBox[
{"10"," ","\[Tau]"}]}],")"}]," ",SuperscriptBox[RowBox[{"(",RowBox[{RowBox[{RowBox[
{"-","\[Alpha]"}]," ",RowBox[{"(",RowBox[{"u","+","\[CapitalDelta]"}],")"}]}],"+","\[Tau]"}],")"}],
"2"]}]}],")"}]}],RowBox[{RowBox[{"(",RowBox[{"u","+","\[CapitalDelta]"}],")"}]," ",SuperscriptBox[
"\[Lambda]","2"]}]]}],StandardForm],"Output",CellLabel -> "Out[13]= ",ExpressionUUID -> "54717119-f9bd-4a84-8d93-82b1a6a91373"]},
Open],ExpressionUUID -> "840fb983-2c26-4138-ac2d-f703b7841a0f"],Cell[
BoxData[""],
"Input",ExpressionUUID -> "32b27481-8782-4203-9585-0a3764113901"]},StyleDefinitions -> "Default.nb",
FrontEndVersion -> "12.2 for Wolfram Cloud 1.58 (March 31, 2021)"]